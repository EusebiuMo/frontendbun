import React, { useState } from "react";
function Form() {
  let [first, setFirst] = useState("");
  let [last, setLast] = useState("");
  function handleSubmit() {
    fetch("/instructor/add", {
      method: "POST",
      body: JSON.stringify({
        firstName: first,
        lastName: last,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  }
  return (
    <form className="Form">
      <label htmlFor="input1">
        <h2>FirstName</h2>{" "}
      </label>
      <input
        type="Text"
        id="input1"
        onChange={(e) => setFirst(e.target.value)}
      ></input>
      <label htmlFor="input2">
        <h2>LastName </h2>
      </label>
      <input
        type="Text"
        id="input2"
        onChange={(e) => setLast(e.target.value)}
      ></input>
      {/* <div className="Options">
        <select>
          {categoryItemData.map((item, index) => {
            return (
              <option value={item}>
                <center>{item}</center>
              </option>
            );
          })}
        </select>
      </div> */}
      <div className="SubmitButton">
        <input
          type="submit"
          onClick={(e) => {
            handleSubmit();
            e.preventDefault();
          }}
        ></input>
      </div>
    </form>
  );
}

export default Form;
