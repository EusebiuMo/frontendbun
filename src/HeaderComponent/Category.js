import React, { useState } from "react";
import Subcategory from "../Pop-upComponent/Subcategory";
function Category() {
  let [isSubcategory, SetIsSubcategory] = useState(false);
  return (
    <div
      className="Category HeaderComponent"
      onMouseOver={() => {
        SetIsSubcategory(true);
      }}
      onMouseOut={() => {
        SetIsSubcategory(false);
      }}
    >
      <h2>
        <center>Category</center>{" "}
      </h2>
      <Subcategory
        nameClass={
          isSubcategory ? "ShowSubcategory Subcategory" : "Subcategory"
        }
      ></Subcategory>
    </div>
  );
}

export default Category;
