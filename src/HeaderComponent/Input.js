import React, { useEffect, useState } from "react";
import SearchResult from "../Pop-upComponent/SearchResult";
import axios from "axios";
function Input() {
  let [isSearchResult, setIsSearchResult] = useState(false);
  let [data, setData] = useState([]);
  let [searchValue, setSearchValue] = useState("");
  useEffect(() => {
    fetch("/course/suggestion", {
      method: "POST",
      body: JSON.stringify({
        title: searchValue,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((resp) => resp.json())
      .then((apiData) => setData(apiData));
  }, [searchValue]);
  return (
    <div
      className="HeaderComponent Search"
      onMouseOver={() => {
        setIsSearchResult(true);
      }}
      onMouseOut={() => {
        setIsSearchResult(false);
      }}
    >
      <center>
        <input
          type="text"
          className="Input"
          placeholder="search for something"
          onChange={(e) => setSearchValue(e.target.value)}
        ></input>
      </center>
      <SearchResult
        nameClass={isSearchResult ? "SearchResult" : "SearchResult NotShown"}
        data={data}
      ></SearchResult>
    </div>
  );
}

export default Input;
