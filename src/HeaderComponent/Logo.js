import React from "react";

function Logo() {
  return (
    <div className="Logo HeaderComponent">
      <h1>
        <center>MyShop</center>
      </h1>
    </div>
  );
}

export default Logo;
