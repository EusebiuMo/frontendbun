import React, { useState } from "react";
import optionData from "../Data/OptionData";
import Suboption from "../Pop-upComponent/Suboption";
function Option({ index }) {
  let [isSuboption, SetIsSuboption] = useState(false);
  return (
    <div
      className="Option HeaderComponent"
      onMouseOver={() => SetIsSuboption(true)}
      onMouseOut={() => SetIsSuboption(false)}
    >
      <h3>
        <center>{optionData[index].title}</center>
      </h3>
      <Suboption
        index={index}
        nameClass={isSuboption ? "Suboption  ShowSuboption" : "Suboption"}
      ></Suboption>
    </div>
  );
}

export default Option;
