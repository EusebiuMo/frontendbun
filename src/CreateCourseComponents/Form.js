import React, { useState } from "react";
function Form() {
  let [ttitle, setTtitle] = useState("");
  let [ddescription, setDdescription] = useState("");
  function handleSubmit() {
    fetch("/course/add", {
      method: "POST",
      body: JSON.stringify({
        title: ttitle,
        description: ddescription,
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    });
  }
  return (
    <form className="Form">
      <label htmlFor="input3">
        <h2>Title</h2>
      </label>
      <input
        type="Text"
        id="input3"
        onChange={(e) => setTtitle(e.target.value)}
      ></input>
      <label htmlFor="input4">
        <h2>Description</h2>
      </label>
      <textarea
        id="input4"
        onChange={(e) => setDdescription(e.target.value)}
      ></textarea>
      <div className="SubmitButton">
        <input
          type="submit"
          onClick={(e) => {
            handleSubmit();
            e.preventDefault();
          }}
        ></input>
      </div>
    </form>
  );
}

export default Form;
