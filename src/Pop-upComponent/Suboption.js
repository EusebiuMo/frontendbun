import React from "react";
import suboptionData from "../Data/SuboptionData";
function Suboption({ index, nameClass }) {
  return (
    <div className={nameClass}>
      <center>
        <h3>{suboptionData[index].description}</h3>{" "}
      </center>
      <br></br>
      <center>
        {" "}
        <h3>
          <a href="">{suboptionData[index].buttonText}</a>
        </h3>
      </center>
    </div>
  );
}

export default Suboption;
