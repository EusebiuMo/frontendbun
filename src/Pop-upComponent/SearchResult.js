import React, { useEffect, useState } from "react";
function SearchResult({ nameClass, data }) {
  return (
    <div className={nameClass}>
      {data
        .filter((element, index) => index <= 5)
        .map((element, index) => {
          return (
            <div className="SearchResultItem">
              <center>
                <h2>{element.title}</h2>
              </center>
            </div>
          );
        })}
    </div>
  );
}
export default SearchResult;
