import React, { useState } from "react";
import categoryItemData from "../Data/CategoryItemData";
import AdditionalSubcategory from "./AdditionalSubcategory";
function Subcategory({ nameClass }) {
  let [isAdditionalSubcategory, setIsAdditionalSubcategory] = useState(false);
  let [index, SetIndex] = useState(0);
  return (
    <div className={nameClass}>
      {categoryItemData.map((item, index) => {
        return (
          <div
            className="SubcategoryItem"
            key={index}
            onMouseOver={() => {
              setIsAdditionalSubcategory(true);
              SetIndex(index);
            }}
          >
            <h3>
              <center>{item}</center>
            </h3>
          </div>
        );
      })}
      <AdditionalSubcategory
        nameClass={
          isAdditionalSubcategory
            ? "AdditionalSubcategory"
            : " AdditionalSubcategory NotShown"
        }
        index={index}
      ></AdditionalSubcategory>
    </div>
  );
}
export default Subcategory;
