import React, { useState } from "react";
import AdditionalSubcategoryItemData from "../Data/AdditionalSubcategoryItemData";
function AdditionalSubcategory({ nameClass, index }) {
  return (
    <div className={nameClass}>
      {AdditionalSubcategoryItemData[index].map((item) => (
        <div className="AdditionalSubcategoryItem">
          <h3>
            <center>{item}</center>
          </h3>
        </div>
      ))}
    </div>
  );
}

export default AdditionalSubcategory;
