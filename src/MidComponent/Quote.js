import React from "react";
import quoteData from "../Data/QuoteData";
function Quote() {
  let number = Math.floor(Math.random() * 10);
  return (
    <div className="MidComponent Quote">
      <h1>
        <center>{quoteData[number].quote}</center>
      </h1>
      <h2>
        <center>{quoteData[number].author}</center>
      </h2>
    </div>
  );
}

export default Quote;
