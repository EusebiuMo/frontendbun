import { doGet } from "./fetch-wrapper";

export const getCourses = () => {
  return doGet("/course/all").then((response) => response.json());
};
