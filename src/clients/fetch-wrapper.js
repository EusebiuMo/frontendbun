

const loginUrl = window.location.origin + '/login';

export const STATUS_UNAUTHORIZED = 401;
export const STATUS_FORBIDDEN = 403;
export const STATUS_CONFLICT = 409;
export const STATUS_NOT_FOUND = 404;
export const STATUS_INVALID_INPUT = 400;
export const STATUS_UNPROCESSABLE_ENTITY = 422;
export const STATUS_OK = 200;
export const STATUS_CREATED = 201;
export const STATUS_NO_CONTENT = 204;
export const STATUS_MULTIPLE_CHOICES = 300;

// Added so application will work on edge - for more details check (the set-cookie header on response issued an extra wrong request for edge)
// https://stackoverflow.com/questions/46288437/set-cookie-header-has-no-effect-about-using-cookies-cors-including-for-local
function edgeFix(requestBody) {
    // return Object.assign(requestBody, {credentials: 'include'});
    return requestBody;
}

function handleResponseErrors(response) {
    // if (response.redirected && loginUrl === response.url) {
    //     window.location.assign("/");
    //     window.location.reload();
    //
    // } else {
        return response;
    // }
}

function doFetch(url, body) {
    return fetch(url, edgeFix(body))
        .then(handleResponseErrors);
}

export function doGet(url, headers = {}) {
    return doFetch(url, {
        method: 'GET',
        headers: mergeHeaders(headers)
    });
}

export function doPost(url, data, headers = {}) {
    return doFetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: mergeHeaders(headers)
    });
}

export function doPut(url, data, headers = {}) {
    return doFetch(url, {
        method: 'PUT',
        body: JSON.stringify(data),
        headers: mergeHeaders(headers)
    });
}

export function doDelete(url, headers = {}) {
    return doFetch(url, {
        method: 'DELETE',
        headers: mergeHeaders(headers)
    });
}

export function mergeHeaders(headers = {}) {

    if (!headers['Content-Type']) {
        headers['Content-Type'] = 'application/json';
    }

    // let authentication = getSessionAuthentication();

    // if (!headers['AuthorizationToken'] && authentication !== null) {
    //     headers['AuthorizationToken'] = authentication.tokenType + " " + authentication.token;
    // }

    return headers;
}

// function getSessionAuthentication() {
//
//     const state = App.store.getState();
//
//     if (state !== undefined && state.session !== undefined && !isNullOrUndefined(state.session.authentication)) {
//         return state.session.authentication;
//     }
//
//     return null;
// }

