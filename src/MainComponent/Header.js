import React from "react";
import Input from "../HeaderComponent/Input";
import Logo from "../HeaderComponent/Logo";
import optionData from "../Data/OptionData";
import Category from "../HeaderComponent/Category";
import Option from "../HeaderComponent/Option";
function Header() {
  return (
    <div className="Header">
      <Logo></Logo>
      <Category></Category>
      <Input></Input>
      {optionData.map((item, index) => {
        return <Option index={index} key={index}></Option>;
      })}
    </div>
  );
}
export default Header;
