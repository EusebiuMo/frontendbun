import React from "react";
import Form from "../CreateInstructorComponents/Form";

function CreateInstructor() {
  return (
    <div className="CreateInstructor">
      <center>
        <br></br>
        <h1>BECOME AN INSTRUCTOR</h1>
      </center>
      <Form></Form>
    </div>
  );
}

export default CreateInstructor;
