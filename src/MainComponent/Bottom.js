import React, { useEffect, useState } from "react";
import Course from "../BottomComponent/Course";
import { getCourses } from "../clients/httpClient";
import axios from "axios";
function Bottom() {
  let [data, setData] = useState([]);
  let [isShown, setIsShown] = useState(false);
  useEffect(() => {
    fetch("/course/all", {
      headers: {
        Authorization: "Basic am9objpwYXNz",
      },
    })
      .then((resp) => resp.json())
      .then((apiData) => setData(apiData));
  }, []);
  return (
    <>
      <div className={isShown ? "Bottom" : " Bottom NotShown"}>
        {data
          .filter((element, index) => index >= 6 && index <= 11)
          .map((element, index) => {
            return (
              <Course
                title={element.title}
                instructorName={element.instructorName}
                rating={element.rating}
                description={element.description}
              ></Course>
            );
          })}
      </div>
      <div className="Bottom">
        {data
          .filter((element, index) => index < 6)
          .map((element, index) => {
            return (
              <Course
                title={element.title}
                instructorName={element.instructorName}
                rating={element.rating}
                description={element.description}
              ></Course>
            );
          })}
        <input
          type="submit"
          className="SeeMore"
          value={isShown ? "SeeLess" : "SeeMore"}
          onClick={() => setIsShown(!isShown)}
        ></input>
      </div>
    </>
  );
}
export default Bottom;
