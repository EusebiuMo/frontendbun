import React, { useEffect, useState } from "react";
import { getCourses } from "../clients/httpClient";
import AdditionalInfo from "./AdditionalInfo";
let titles = {};
function Course({ title, instructorName, rating, description }) {
  // let [state, setState] = useState("");
  // useEffect(() => {
  //   fetch("/course/all").then((response) => console.log(response.json));
  // });
  let [isAdditional, setIsAdditional] = useState(false);
  return (
    <div
      className="BottomComponent Course"
      onMouseOver={() => setIsAdditional(true)}
      onMouseOut={() => setIsAdditional(false)}
    >
      <div className="Image">
        <img src="https://www.tutorialrepublic.com/lib/images/javascript-illustration.png"></img>
      </div>
      <div className="Title">
        <center>
          <h3>{title}</h3>
        </center>
      </div>
      <div className="Rating">
        <center>
          <h3>Rating: {rating}</h3>{" "}
        </center>
      </div>
      <div className="InstructorName">
        <center>
          <h3>{instructorName}</h3>
        </center>
      </div>
      <AdditionalInfo
        nameClass={isAdditional ? "AdditionalInfo" : "AdditionalInfo NotShown"}
        description={description}
      ></AdditionalInfo>
    </div>
  );
}

export default Course;
