import React, { useEffect, useState } from "react";

function AdditionalInfo({ nameClass, description }) {
  return (
    <div className={nameClass}>
      <center>
        <h2>{description}</h2>
      </center>
    </div>
  );
}

export default AdditionalInfo;
