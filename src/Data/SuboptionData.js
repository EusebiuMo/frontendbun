let suboptionData = [
  {
    description:
      "Get your team access to over 17,000 top Udemy courses, anytime, anywhere.",
    buttonText: "Try MyShopBusiness",
  },
  {
    description:
      "Turn what you know into an opportunity and reach millions around the world.",
    buttonText: "Learn more",
  },
  { description: "Go to your course", buttonText: "Go to my learning " },
];

export default suboptionData;
