let AdditionalSubcategoryItemData = [
  ["web", "mobile", "programming", "game", "data", "testing"],
  ["entrepreneurship", "managment", "leadership", "sales", "strategy", "team"],
  ["finance", "bytcoin", "trading", "cryptocurrrency", "bookkeeping"],
  ["hardware", "network", "security", "operation system", "servers"],
  ["web design", "graphic", "design tools", "3D", "animations"],
  [
    "digital marketing",
    "search engine optimization",
    "branding",
    "social media",
    "marketing fundamentals",
  ],
];

export default AdditionalSubcategoryItemData;
