let quoteData = [
  {
    quote: "Change is the end result of all true learning",
    author: "Leo Buscaglia",
  },
  {
    quote:
      "Education is the passport to the future, for tomorrow belongs to those who prepare for it today.",
    author: "Malcolm X",
  },
  {
    quote: "An investment in knowledge pays the best interest",
    author: "Benjamin Franklin",
  },
  {
    quote: "The roots of education are bitter, but the fruit is sweet",
    author: " Aristotle",
  },
  {
    quote:
      "Education is what remains after one has forgotten what one has learned in school.",
    author: " Albert Einstein",
  },
  {
    quote:
      "The more that you read, the more things you will know, the more that you learn, the more places you’ll go.",
    author: " Dr. Seuss",
  },
  {
    quote:
      "Live as if you were to die tomorrow. Learn as if you were to live forever",
    author: "Mahatma Gandhi",
  },
  {
    quote:
      "Education without values, as useful as it is, seems rather to make man a more clever devil",
    author: "C.S. Lewis",
  },
  {
    quote: "The learning process continues until the day you die.",
    author: " Kirk Douglas ",
  },
  {
    quote: "Education is not the filling of a pail, but the lighting of a fire",
    author: "W.B. Yeats",
  },
];

export default quoteData;
