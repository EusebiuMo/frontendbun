import "./App.css";
import Header from "./MainComponent/Header";
import Mid from "./MainComponent/Mid";
import Bottom from "./MainComponent/Bottom";
import CreateInstructor from "./MainComponent/CreateInstructor";
import CreateCourse from "./MainComponent/CreateCourse";
function App() {
  return (
    <div>
      <Header></Header>
      <Mid></Mid>
      <Bottom></Bottom>
      <CreateInstructor></CreateInstructor>
      <CreateCourse></CreateCourse>
    </div>
  );
}

export default App;
